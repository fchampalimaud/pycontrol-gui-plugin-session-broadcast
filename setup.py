#!/usr/bin/python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
import re

version = ''
with open('pycontrolgui_plugin_session_broadcast/__init__.py', 'r') as fd:
    version = re.search(r'^__version__\s*=\s*[\'"]([^\'"]*)[\'"]',
                        fd.read(), re.MULTILINE).group(1)

if not version:
    raise RuntimeError('Cannot find version information')


setup(
    name='pycontrol-gui-plugin-session-broadcast',
    version=version,
    description="""UDP client plugin""",
    author='Carlos Mão de Ferro, Ricardo Jorge Vieira Ribeiro',
    author_email='cajomferro@gmail.com, ricardojvr@gmail.com',
    license='Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>',
    url='https://bitbucket.org/fchampalimaud/pycontrolgui',

    include_package_data=True,
    packages=find_packages(exclude=['contrib', 'docs', 'tests', 'examples', 'deploy', 'reports']) ,

    package_data={'pycontrolgui_plugin_session_broadcast': [
            'resources/*.*',
        ]
    }, 
)