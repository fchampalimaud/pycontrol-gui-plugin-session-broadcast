# PyControl Plugin - UDP Client

Send UDP state changes from board session.
	
## How to install

First generate package (or download from the 'downloads' section on bitbucket):

	python3 setup.py sdist --formats=zip
	

Then copy this folder inside the pycontrolgui/plugins folder.