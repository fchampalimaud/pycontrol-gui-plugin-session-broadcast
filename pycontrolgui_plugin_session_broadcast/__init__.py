# !/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrol-udp-client

"""

__version__ = "1.0"
__author__ = "Carlos Mão de Ferro, Ricardo Jorge Vieira Ribeiro"
__credits__ = ["Carlos Mão de Ferro", "Ricardo Ribeiro"]
__license__ = "Copyright (C) 2007 Free Software Foundation, Inc. <http://fsf.org/>"
__maintainer__ = ["Carlos Mão de Ferro", "Ricardo Ribeiro"]
__email__ = ["cajomferro@gmail.com", "ricardojvr@gmail.com"]
__status__ = "Development"


from pysettings import conf; conf+='pycontrolgui_plugin_session_broadcast.settings'
from pycontrolgui_plugin_session_broadcast.udp_client import UDPClient
