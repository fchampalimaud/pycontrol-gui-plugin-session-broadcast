# !/usr/bin/python3
# -*- coding: utf-8 -*-
import logging; logger = logging.getLogger(__name__)
from pycontrolgui_plugin_session_broadcast.udp_client import UDPClient
from pysettings import conf

class SessionTreeNode(object):
	
	def create_treenode(self, tree):
		node = super(SessionTreeNode, self).create_treenode(tree)
		#print(conf.SESSIONBROADCAST_PLUGIN_ICON)
		tree.addPopupMenuOption('UDP Client', self.__open_udp_client_plugin, item=self.node, icon=conf.SESSIONBROADCAST_PLUGIN_ICON)
		return node


	def __open_udp_client_plugin(self):
		if not hasattr(self, 'session_udp_client_plugin'):
			self.session_udp_client_plugin = UDPClient(self)
			self.session_udp_client_plugin.show()			
			self.session_udp_client_plugin.subwindow.resize(*conf.SESSIONBROADCAST_PLUGIN_WINDOW_SIZE)         
		else:
			self.session_udp_client_plugin.show()


	def remove(self):
		if hasattr(self, 'session_udp_client_plugin'): self.mainwindow.mdi_area -= self.session_udp_client_plugin
		super(SessionTreeNode, self).remove()


	@property
	def name(self): return super(SessionTreeNode, self.__class__).name.fget(self)
	@name.setter
	def name(self, value):
		super(SessionTreeNode, self.__class__).name.fset(self, value)
		if hasattr(self, 'session_udp_client_plugin'): self.session_udp_client_plugin.title = value