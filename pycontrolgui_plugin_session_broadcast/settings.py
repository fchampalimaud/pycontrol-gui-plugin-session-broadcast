import os

SESSIONBROADCAST_PLUGIN_ICON = os.path.join(os.path.dirname(__file__), 'resources', 'broadcast.png')

SESSIONBROADCAST_PLUGIN_WINDOW_SIZE = 200, 200

SESSIONHISTORY_PLUGIN_REFRESH_RATE = 1000 # in milliseconds