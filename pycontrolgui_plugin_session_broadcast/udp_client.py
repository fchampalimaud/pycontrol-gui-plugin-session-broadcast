#!/usr/bin/python3
# -*- coding: utf-8 -*-

""" pycontrolgui_plugin_session_broadcast.udp_client"""

import logging
import socket

from pysettings import conf

from PyQt4.QtCore import QTimer, QEventLoop
from pyforms import BaseWidget
from pyforms.Controls import ControlText
from pyforms.Controls import ControlButton

from pycontrolapi.models.board.com.state_entry import StateEntry
from pycontrolapi.exceptions.run_setup import RunSetupError

logger = logging.getLogger(__name__)


class UDPClient(BaseWidget):
	""" Show all boxes live state for an experiment"""

	PLUGIN_NAME = 'UDP Client'

	# WINDOW_SIZE = settings.WINDOW_SESSION_EVENTS_PLOT_SIZE  # @UndefinedVariable

	def __init__(self, session):
		"""
		:param session_window: session window reference
		:type session_window: pycontrolgui.windows.detail.entities.session_window.SessionWindow
		"""
		BaseWidget.__init__(self, session.name)

		self.session = session

		self._history_index = 0
		self._last_event = None

		self._text_host = ControlText('Host IP', 'localhost')
		self._text_port = ControlText('Port', '9999')
		self._button_conn_toogle = ControlButton("Start logging", checkable=True)
		self._button_conn_toogle.value = self._conn_toogle

		self._formset = [(' ', '_text_host', ' '), (' ', '_text_port', ' '), (' ', ' ', '_button_conn_toogle')]

		# SOCK_DGRAM is the socket type to use for UDP sockets
		# self.is_connected = False
		# self.disable_timer()
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

		self._timer = QTimer()
		self._timer.timeout.connect(self.read_message_queue)

	def show(self):
		# Prevent the call to be recursive because of the mdi_area
		if hasattr(self, '_show_called'):
			BaseWidget.show(self)
			return
		self._show_called = True
		self.mainwindow.mdi_area += self
		del self._show_called

	def hide(self):
		self._timer.stop()

	def beforeClose(self):
		self._timer.stop()
		return False

	def _conn_toogle(self, button_state):
		"""
		Start/Stop sending data to remote host
		:param button_state: if the button is selected or unselected
		:type button_state: boolean
		"""
		# TODO: validate input fields
		self.is_connected = button_state
		if button_state is True:
			self._button_conn_toogle.label = 'Stop logging'
			self._timer.start(conf.SESSIONHISTORY_PLUGIN_REFRESH_RATE)
		else:
			self._button_conn_toogle.label = 'Start logging'
			self._timer.stop()

	def read_message_queue(self, update_gui=False):
		""" Update board queue and retrieve most recent messages """
		if self.is_connected:
			messages_history = self.session.messages_history
			recent_history = messages_history[self._history_index:]
			states = self.session.setup.board_task.states

			try:
				for message in recent_history:
					if isinstance(message, StateEntry):
						logger.debug("New message %s", message._state_id)
						self._send_message(str(message._state_id))

					self._history_index += 1


			except RunSetupError as err:
				logger.error(str(err), exc_info=True)
				if hasattr(self, '_timer'):
					self._timer.stop()

		else:
			logger.debug("Press button to start sending commands")

	def _send_message(self, data):
		try:
			self.sock.sendto(bytes(data + "\n", "utf-8"), (str(self._text_host.value), int(self._text_port.value)))
		except Exception as err:
			logger.error(str(err), exc_info=True)

	@property
	def sock(self):
		""" Socket object"""
		return self._sock

	@sock.setter
	def sock(self, value):
		"""
		Set socket object
		:param value: socket reference
		:type value: socket.socket
		"""
		self._sock = value

	@property
	def is_connected(self):
		""" Socket object"""
		return self._is_connected

	@is_connected.setter
	def is_connected(self, value):
		"""
		Set socket object
		:param value: socket reference
		:type value: socket.socket
		"""
		self._is_connected = value

	@property
	def mainwindow(self):
		return self.session.mainwindow

	@property
	def title(self):
		return BaseWidget.title.fget(self)

	@title.setter
	def title(self, value):
		BaseWidget.title.fset(self, 'UDP Client: {0}'.format(value))

# Execute the application
# if __name__ == "__main__":
#    app.startApp(UDPClient)
